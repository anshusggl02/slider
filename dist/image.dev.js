"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Image = function Image(image, title, description) {
  _classCallCheck(this, Image);

  this.image = image;
  this.title = title;
  this.description = description;
};