class Slider {
    constructor() {
        this.slide = [];
        this.index = 0;

    }

    addImage = (addSlider) => {

        this.slide.push(addSlider);

    }

    nextSlider = () => {
        this.index++;
        if (this.index >= this.slide.length) {
            this.index = 0;
        }

        console.log(this.index)
        this.replaceImage(this.slide[this.index]);

    }

    prevSlider = () => {
        this.index--;
        if (this.index < 0) {
            this.index = this.slide.length - 1;
        }


        this.replaceImage(this.slide[this.index]);

    }




    replaceImage = (slide) => {
        document.getElementById("output").src = slide.image;
        document.getElementById("titleId").innerHTML = slide.title;
        document.getElementById("descriptionId").innerHTML = slide.description;
    }


    button = () => {
        const sliderElement = document.createElement("div");
        const previousbtn = document.createElement("button");
        previousbtn.innerHTML = "previous";
        sliderElement.appendChild(previousbtn);
        previousbtn.addEventListener("click", this.prevSlider);

        const nextbtn = document.createElement("button");
        nextbtn.innerHTML = "next";
        sliderElement.appendChild(nextbtn);
        document.body.appendChild(sliderElement);
        nextbtn.addEventListener("click", this.nextSlider);
    }


    addSlider = () => {
        const slide = this.slide[this.index];
        this.replaceImage(slide);
        if (this.slide.length == 1) {
            this.button();
        }

    }


}

const slider = new Slider();